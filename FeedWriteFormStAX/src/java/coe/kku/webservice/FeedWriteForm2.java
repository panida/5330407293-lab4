package coe.kku.webservice;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import javax.xml.stream.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@WebServlet(name = "FeedWriteForm2", urlPatterns = {"/FeedWriteFormStAX"})
public class FeedWriteForm2 extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String title = request.getParameter("title");
        String link = request.getParameter("link");
        String description = request.getParameter("description");

        ArrayList<String> oldTitle = new ArrayList<String>();
        ArrayList<String> oldDes = new ArrayList<String>();
        ArrayList<String> oldLink = new ArrayList<String>();
        ArrayList<String> oldPubDate = new ArrayList<String>();

        try {
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builderfactory = factory.newDocumentBuilder();
            String urlLoc = "http://localhost:8080/FeedWriteFormStAX/FeedWriteForm2.xml";

            URL url = new URL(urlLoc);
            Document doc = builderfactory.parse(url.openStream());

            NodeList item = doc.getElementsByTagName("item");

            for (int i = 0; i < item.getLength(); i++) {
                Node node = item.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element getElement = (Element) node;
                    String getOldTitle = getElement.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldDes = getElement.getElementsByTagName("description").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldLink = getElement.getElementsByTagName("link").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldPubDate = getElement.getElementsByTagName("pubDate").item(0).getChildNodes().item(0).getNodeValue();
                    oldTitle.add(getOldTitle);
                    oldDes.add(getOldDes);
                    oldLink.add(getOldLink);
                    oldPubDate.add(getOldPubDate);
                }
            }
        } catch (Throwable e) {
        }

        try {
            new FeedWriteForm2().createRssDoc(title, description, link, oldTitle, oldDes, oldLink, oldPubDate);
            out.println("<b>"
                    + "<a href=\'http://localhost:8080/FeedWriteFormStAX/FeedWriteForm2.xml\'> Rss Feed </a>"
                    + " was create successfully</b> ");
        } catch (Exception e) {
            System.out.println();
        }
    }

    public void createRssDoc(String title, String description, String link, ArrayList<String> oldTitle, ArrayList<String> oldDes, ArrayList<String> oldLink, ArrayList<String> oldPubDate) throws Exception {
        String filename = "D:/XML/lab/lab4/FeedWriteFormStAX/web/FeedWriteForm2.xml";
        File file = new File(filename);

        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        
        xtw.writeStartDocument();

        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");

        // rss/channel
        xtw.writeStartElement("channel");

        // rss/channel/title
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();//end title

        // rss/channel/description
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();//end description

        // rss/channel/link
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();//end link

        // rss/channel/lang
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();//end lang

        // element/rss/channel/old item
        for (int i = 0; i < oldTitle.size(); i++) {
            xtw.writeStartElement("item");
            
            // rss/channel/item/old title
            xtw.writeStartElement("title");
            xtw.writeCharacters(oldTitle.get(i));
            xtw.writeEndElement();

            // rss/channel/item/old description
            xtw.writeStartElement("description");
            xtw.writeCharacters(oldDes.get(i));
            xtw.writeEndElement();

            // rss/channel/item/old link
            xtw.writeStartElement("link");
            xtw.writeCharacters(oldLink.get(i));
            xtw.writeEndElement();

            // rss/channel/item/old pubdate
            xtw.writeStartElement("pubDate");
            xtw.writeCharacters(oldPubDate.get(i));
            xtw.writeEndElement();

            xtw.writeEndElement();//end	element	item
        }

        // element/rss/channel/new item
        xtw.writeStartElement("item");

        // element/rss/channel/new title
        xtw.writeStartElement("title");
        xtw.writeCharacters(title);
        xtw.writeEndElement();

        // element/rss/channel/new description
        xtw.writeStartElement("description");
        xtw.writeCharacters(description);
        xtw.writeEndElement();

        // element/rss/channel/new link
        xtw.writeStartElement("link");
        xtw.writeCharacters(link);
        xtw.writeEndElement();

        // element/rss/channel/new pubdate
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();

        xtw.writeEndElement();//end element item
        xtw.writeEndElement();//end element channel
        xtw.writeEndElement();//end element rss

        // element/rss/channel/new item
        xtw.writeEndDocument();
        xtw.flush();    
        xtw.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }//	</editor-fold>  
}
