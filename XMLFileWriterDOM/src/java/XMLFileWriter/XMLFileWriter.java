package XMLFileWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class XMLFileWriter {

    public static void main(String argv[])
            throws ParserConfigurationException, TransformerConfigurationException, FileNotFoundException, TransformerException {

        //create Document DOM
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builderfactory = factory.newDocumentBuilder();
        Document doc = builderfactory .newDocument();

        //create element
        Element quotes = doc.createElement("quotes");
        Element quote = doc.createElement("quote");

        Element words = doc.createElement("words");
        Element by = doc.createElement("by");

        //create text 
        Text wordsText = doc.createTextNode("Time is more value than money. You can get more money, but you cannat get more time");
        Text byText = doc.createTextNode("Jim Rohn");

        //add file text into element
        words.appendChild(wordsText);
        by.appendChild(byText);
        quote.appendChild(words);
        quote.appendChild(by);

        quotes.appendChild(quote);
        doc.appendChild(quotes);

        Element quote2 = doc.createElement("quote");

        Element words2 = doc.createElement("words");
        Element by2 = doc.createElement("by");

        Text wordsText2 = doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็ก ๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
        Text byText2 = doc.createTextNode("ว. วชิรเมธี");

        words2.appendChild(wordsText2);
        by2.appendChild(byText2);
        quote2.appendChild(words2);
        quote2.appendChild(by2);

        quotes.appendChild(quote2);

        //transform file DOM to XML
        String filename = "quotes.xml";
        File file = new File(filename);
        OutputStream xmlfile = new FileOutputStream(file);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.transform(new DOMSource(doc), new StreamResult(xmlfile));
    }
}