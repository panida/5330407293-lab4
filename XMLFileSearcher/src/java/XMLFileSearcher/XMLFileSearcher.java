package XMLFileSearcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLFileSearcher {

    public static void main(String argv[]) throws FileNotFoundException {

        String keyword;
        String key = "keyword.txt";
        File xmldata = new File("quotes.xml");
        Scanner scan = new Scanner(new FileInputStream(key));


        try {
            while (scan.hasNextLine()) {
                keyword = scan.nextLine();
                //System.out.println(keyword);
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder parser = factory.newDocumentBuilder();
                Document doc = parser.parse(xmldata);

                NodeList items = doc.getElementsByTagName("quote");

                for (int i = 0; i < items.getLength(); i++) {
                    Element item = (Element) items.item(i);

                    if (getElemVal(item, "by").toLowerCase().contains(keyword.toLowerCase())) {
                        System.out.println(getElemVal(item, "words")+ " by " + getElemVal(item, "by"));
                    }
                }
            }
        } catch (Exception e) {
            System.out.print(e);
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}
