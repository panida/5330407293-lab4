package XMLFileSearcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XMLFileSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException {

        String keyword;
        String key = "keyword.txt";
        File xmldata = new File("quotes.xml");

        boolean quoteFound = false;
        boolean wordsFound = false;
        boolean byFound = false;
        String quote = null;
        String words = null;
        String by = null;
        String eName;
        Scanner scan = new Scanner(new FileInputStream(key));

        try {
            while (scan.hasNext()) {
                keyword = scan.nextLine();
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(xmldata)));
                while (reader.hasNext()) {

                    XMLEvent event = reader.nextEvent();

                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = true;
                        }
                        if (quoteFound && eName.equals("words")) {
                            wordsFound = true;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }
                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = false;
                        }
                        if (quoteFound && eName.equals("words")) {
                            wordsFound = false;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }
                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;
                        if (byFound) {
                            by = characters.getData();
                            if (by.toLowerCase().contains(keyword.toLowerCase())) {
                                System.out.println(words + " by " + by);
                            }
                        }
                        if (wordsFound) {
                            words = characters.getData();
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.print(e);
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}
