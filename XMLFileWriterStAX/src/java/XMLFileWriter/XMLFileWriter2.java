package XMLFileWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLFileWriter2 {

    public static void main(String argv[]) throws FileNotFoundException {
        try {
            String filename = "quotes.xml";
            File file = new File(filename);
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));
            xtw.writeStartDocument();
                        
            xtw.writeStartElement("quotes");

            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("Time is more value than money. You can get more money, but you cannat get more time");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement();
            xtw.writeEndElement();

            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็ก ๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement();
            xtw.writeEndElement();

            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("You will never find time for anything. If you want the time, you must make it");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("Charles Buxton");
            xtw.writeEndElement();
            xtw.writeEndElement();

            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("คนส่วนใหญ่เรียกร้องสิทธิมนุษยชน แต่คนมีปัญญาเรียกร้องสิทธิ์ที่จะไม่ทุกข์");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement();
            xtw.writeEndElement();

            xtw.writeEndElement();

            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();


        } catch (Exception e) {
            System.err.println("Exception occurred while running writer samples");
        }

        System.out.println("Done");
    }
}
