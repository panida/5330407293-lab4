package coe.kku.webservice;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import javax.xml.stream.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

@WebServlet(name = "FeedWriteForm", urlPatterns = {"/FeedWriteFormDOM"})
public class FeedWriteForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String title = request.getParameter("title");
        String link = request.getParameter("link");
        String description = request.getParameter("description");

        ArrayList<String> oldTitle = new ArrayList<String>();
        ArrayList<String> oldDes = new ArrayList<String>();
        ArrayList<String> oldLink = new ArrayList<String>();
        ArrayList<String> oldPubDate = new ArrayList<String>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builderfactory = factory.newDocumentBuilder();
            String urlLoc = "http://localhost:8080/FeedWriteFormDOM/FeedWriteForm.xml";

            URL url = new URL(urlLoc);
            Document doc = builderfactory.parse(url.openStream());

            NodeList item = doc.getElementsByTagName("item");

            for (int i = 0; i < item.getLength(); i++) {
                Node node = item.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element getElement = (Element) node;
                    String getOldTitle = getElement.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldDes = getElement.getElementsByTagName("description").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldLink = getElement.getElementsByTagName("link").item(0).getChildNodes().item(0).getNodeValue();
                    String getOldPubDate = getElement.getElementsByTagName("pubDate").item(0).getChildNodes().item(0).getNodeValue();
                    oldTitle.add(getOldTitle);
                    oldDes.add(getOldDes);
                    oldLink.add(getOldLink);
                    oldPubDate.add(getOldPubDate);
                }
            }
        } catch (Throwable e) {
        }

        try {
            createXML(title, description, link, oldTitle, oldDes, oldLink, oldPubDate);
            out.println("<b><a href=\"http://localhost:8080/FeedWriteFormDOM/FeedWriteForm.xml\">" + "Rss Feed </a > was   create successfully</b >");
        } catch (Exception e) {
            System.out.println();
        }
    }

    public void createXML(String title, String description, String link, ArrayList<String> oldTitle, ArrayList<String> oldDes, ArrayList<String> oldLink, ArrayList<String> oldPubDate)
            throws FileNotFoundException, TransformerConfigurationException, TransformerException, ParserConfigurationException {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Element Mrss = doc.createElement("rss");
        Mrss.setAttribute("version", "2.0");
        Element Mchannel = doc.createElement("channel");

        Element Mtitle = doc.createElement("title");
        Element Mdescription = doc.createElement("description");
        Element Mlink = doc.createElement("link");
        Element Mlang = doc.createElement("lang");

        Text MtitleText = doc.createTextNode("Khon Kean University Rss Feed");
        Text MdescriptionText = doc.createTextNode("Khon Kean University Information News Rss Feed");
        Text MlinkText = doc.createTextNode("http://www.kku.ac.th");
        Text MlangText = doc.createTextNode("en-th");

        Mtitle.appendChild(MtitleText);
        Mdescription.appendChild(MdescriptionText);
        Mlink.appendChild(MlinkText);
        Mlang.appendChild(MlangText);
        
        Mchannel.appendChild(Mtitle);
        Mchannel.appendChild(Mdescription);
        Mchannel.appendChild(Mlink);
        Mchannel.appendChild(Mlang);

        // add old data
        for (int i = 0; i < oldTitle.size(); i++) {

            Element Oitem = doc.createElement("item");
            Element Otitle = doc.createElement("title");
            Element Odescription = doc.createElement("description");
            Element Olink = doc.createElement("link");
            Element OpubDate = doc.createElement("pubDate");

            Text OitemText = doc.createTextNode(oldTitle.get(i));
            Text OdescriptionText = doc.createTextNode(oldDes.get(i));
            Text OlinkText = doc.createTextNode(oldLink.get(i));
            Text OpubDateText = doc.createTextNode(oldPubDate.get(i));

            Otitle.appendChild(OitemText);
            Odescription.appendChild(OdescriptionText);
            Olink.appendChild(OlinkText);
            OpubDate.appendChild(OpubDateText);

            Oitem.appendChild(Otitle);
            Oitem.appendChild(Odescription);
            Oitem.appendChild(Olink);
            Oitem.appendChild(OpubDate);

            Mchannel.appendChild(Oitem);
        }

        // add new data
        Element Nitem = doc.createElement("item");
        Element Ntitle = doc.createElement("title");
        Element Ndescription = doc.createElement("description");
        Element Nlink = doc.createElement("link");
        Element NpubDate = doc.createElement("pubDate");

        Text NtitleText = doc.createTextNode(title);
        Text NdescriptionText = doc.createTextNode(description);
        Text NlinkText = doc.createTextNode(link);
        Text NpubDateText = doc.createTextNode((new java.util.Date()).toString());

        Ntitle.appendChild(NtitleText);
        Ndescription.appendChild(NdescriptionText);
        Nlink.appendChild(NlinkText);
        NpubDate.appendChild(NpubDateText);

        Nitem.appendChild(Ntitle);
        Nitem.appendChild(Ndescription);
        Nitem.appendChild(Nlink);
        Nitem.appendChild(NpubDate);

        Mchannel.appendChild(Nitem);
        Mrss.appendChild(Mchannel);

        doc.appendChild(Mrss);

        // write to file
        String filename = "D:/XML/lab/lab4/FeedWriteFormDOM/web/FeedWriteForm.xml";
        File file = new File(filename);

        OutputStream os = new FileOutputStream(file);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.transform(new DOMSource(doc), new StreamResult(os));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
